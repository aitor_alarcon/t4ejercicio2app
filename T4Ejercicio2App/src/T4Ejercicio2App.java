/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T4Ejercicio2App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creo las tres variables y las inicializo
		int N = 10;
		double A = 30.3;
		char C = 'B';
		
		// Guardo el valor n�merico del char escogido anteriormente
		int ascii = C;
		
		// Muestro todos los valores asignados a las variables
		System.out.println("Variable N = " + N);
		System.out.println("Variable A = " + A);
		System.out.println("Variable C = " + C);
		
		// Hago los c�lculos y los muestro
		System.out.println(N + " + " + A + " = " + (N + A));
		System.out.println(A + " - " + N + " = " + (A - N));
		System.out.println("Valor num�rico del car�cter B = " + ascii);
		
	}

}
